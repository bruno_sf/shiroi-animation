import me.ddevil.shiroi.animation.api.rating.Rating;
import me.ddevil.shiroi.animation.internal.rating.InternalAnimationRating;
import me.ddevil.shiroi.animation.internal.rating.InternalUserRating;
import org.json.simple.JSONObject;
import org.junit.Test;

import java.util.UUID;

public class SerializationTest {
    @Test
    public void ratings() throws Exception {
        InternalAnimationRating rating = new InternalAnimationRating("progressiveAnimationTest");
        for (int i = 0; i < 10; i++) {
            rating.plus(new InternalUserRating(Rating.PERFECT, UUID.randomUUID()));
        }
        System.out.println(new JSONObject(rating.serialize()).toJSONString());
    }
}
