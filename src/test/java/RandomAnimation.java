import me.ddevil.shiroi.animation.api.Animation;
import me.ddevil.shiroi.animation.api.ArmorLimb;
import me.ddevil.shiroi.animation.api.ArmorProperty;
import me.ddevil.shiroi.animation.internal.InternalAnimation;
import me.ddevil.shiroi.animation.internal.InternalFrame;
import me.ddevil.shiroi.animation.internal.InternalUser;
import me.ddevil.shiroi.misc.vector.Vector3;
import me.ddevil.shiroi.util.StringUtils;
import org.jfairy.Fairy;

import java.util.Arrays;
import java.util.Random;
import java.util.UUID;

import me.ddevil.shiroi.animation.api.User;

public class RandomAnimation {


    public static final Fairy FAIRY = Fairy.create();

    public static Animation randomAnimation(int maxUser, int frames) {
        Random r = new Random();
        User[] users = new User[maxUser];
        for (int i = 0; i < maxUser; i++) {
            users[i] = new InternalUser(UUID.randomUUID());
        }
        String name = FAIRY.textProducer().word(3);
        InternalAnimation anim = new InternalAnimation(StringUtils.toQualifiedName(name), name, users);
        ArmorLimb[] limbs = ArmorLimb.values();
        ArmorProperty[] props = ArmorProperty.values();
        boolean b = false;
        for (int i = 0; i < frames; i++) {
            InternalFrame frame = new InternalFrame();
            for (int j = 0; j < r.nextInt(limbs.length); j++) {
                ArmorLimb limb = limbs[j];
                Vector3<Integer> pos = new Vector3<>(r.nextInt(360), r.nextInt(360), r.nextInt(360));
                frame.set(limb, pos);
            }
            ArmorProperty prop = props[r.nextInt(props.length)];
            frame.set(prop, b);
            b = !b;
            anim.plus(frame);
        }
        return anim;
    }


}
