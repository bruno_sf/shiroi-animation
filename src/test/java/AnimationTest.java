import me.ddevil.shiroi.animation.api.Animation;
import me.ddevil.shiroi.animation.api.ArmorLimb;
import me.ddevil.shiroi.animation.api.ArmorProperty;
import me.ddevil.shiroi.animation.api.User;
import me.ddevil.shiroi.animation.internal.InternalAnimation;
import me.ddevil.shiroi.animation.internal.InternalFrame;
import me.ddevil.shiroi.animation.internal.InternalUser;
import me.ddevil.shiroi.animation.internal.provider.LocalAnimationProvider;
import me.ddevil.shiroi.misc.vector.Vector3;
import org.jfairy.Fairy;
import org.json.simple.JSONObject;
import org.junit.Test;

import java.io.File;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;

/**
 * Created by Selma on 19/11/16.
 */
public class AnimationTest {

    private static final long ONE_SECOND_IN_TICKS = 20L;
    private static final int TOTAL_TICKS = 2;
    private static final int TOTAL_FRAMES = (int) (TOTAL_TICKS * ONE_SECOND_IN_TICKS);

    @Test
    public void loadingTest() {
        LocalAnimationProvider provider = getProvider();
        System.out.println(new JSONObject(provider.get("randomAnimTest").serialize()).toJSONString());
    }


    private LocalAnimationProvider getProvider() {
        return new LocalAnimationProvider(new File("/work/shiroi-animation/src/test/resources"));
    }

    @Test
    public void progressiveAnimation() {
        LocalAnimationProvider provider = getProvider();
        Animation anim = new InternalAnimation("progressiveAnimTest", "Progressive Animation", new User[]{new InternalUser(UUID.randomUUID())}
        );
        for (int i = 0; i < TOTAL_FRAMES; i++) {
            int angle = (int) (((float) i / TOTAL_FRAMES) * 360);
            System.out.println("Angle for frame " + i + " = " + angle);
            InternalFrame frame = new InternalFrame();
            for (ArmorLimb limb : ArmorLimb.values()) {
                frame.set(limb, new Vector3<>(angle, angle, angle));
            }
            anim.plus(frame);
        }
        provider.save(anim);
    }
}
