import me.ddevil.shiroi.animation.api.Animation;
import me.ddevil.shiroi.animation.api.request.DownloadAnimationRequest;
import me.ddevil.shiroi.animation.api.request.PublishAnimationRequest;
import org.json.simple.JSONObject;
import org.junit.Test;

import java.util.Random;

/**
 * Created by Selma on 21/11/16.
 */
public class RequestTest {
    private static final String animation = "a_leo_dolor";
    private static final int MAX_POSSIBLE_AUTHORS = 4;
    private static final long ONE_SECOND_IN_TICKS = 20L;
    private static final int TOTAL_TICKS = 5;
    private static final int MAX_FRAMES = (int) (TOTAL_TICKS * ONE_SECOND_IN_TICKS);

    @Test
    public void getAnimationTest() {
        try {
            DownloadAnimationRequest.DownloadAnimationResult call = new DownloadAnimationRequest(animation).call();
            Animation animation = call.getAnimation();
            if (call.getAnimation() != null) {
                System.out.println(new JSONObject(call.getAnimation().serialize()).toJSONString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void populate() {
        Random r = new Random();
        for (int i = 0; i < 10; i++) {
            Animation animation = RandomAnimation.randomAnimation(r.nextInt(MAX_POSSIBLE_AUTHORS), r.nextInt(MAX_FRAMES));
            try {
                System.out.println(i + "-result = " + new PublishAnimationRequest(animation).call().wasPublished());
                ;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
