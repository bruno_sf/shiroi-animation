package me.ddevil.shiroi.animation.api.provider

import me.ddevil.shiroi.animation.api.Animation

interface AnimationProvider {

    fun save(animation: Animation);

    operator fun get(animation: String): Animation?;

    fun has(animation: String): Boolean

}