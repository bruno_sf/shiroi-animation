package me.ddevil.shiroi.animation.api.rating

import me.ddevil.shiroi.misc.Serializable
import java.util.SortedSet;

interface AnimationRating : Serializable {

    fun getAnimationName(): String

    fun getRatings(): SortedSet<UserRating>

    operator fun plus(userRating: UserRating);

    fun getAverageScore(): Float

}