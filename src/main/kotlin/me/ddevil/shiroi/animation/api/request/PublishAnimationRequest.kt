package me.ddevil.shiroi.animation.api.request

import com.google.common.net.HttpHeaders
import com.google.common.net.MediaType
import me.ddevil.shiroi.animation.api.Animation
import me.ddevil.shiroi.animation.api.GalleryConstants
import me.ddevil.shiroi.misc.request.Result
import org.apache.http.HttpResponse
import org.apache.http.HttpStatus
import org.apache.http.client.methods.HttpPost
import org.apache.http.client.methods.HttpUriRequest
import org.apache.http.entity.StringEntity
import org.json.simple.JSONObject

class PublishAnimationRequest(private final var animation: Animation) : BaseAnimationRequest<PublishAnimationRequest.PublishAnimationResult>() {
    override fun call1(response: HttpResponse): PublishAnimationResult {
        return PublishAnimationResult(response.statusLine.statusCode == HttpStatus.SC_CREATED)
    }

    override fun createRequest(): HttpUriRequest {

        var request = HttpPost(GalleryConstants.CENTRAL_GALLERY + animation.name)
        request.entity = StringEntity(JSONObject(animation.serialize()).toJSONString())
        request.addHeader(HttpHeaders.CONTENT_TYPE, MediaType.JSON_UTF_8.toString())
        return request
    }


    class PublishAnimationResult : Result {
        constructor(wasPublished: Boolean) {
            this.wasPublished = wasPublished
        }

        private final val wasPublished: Boolean;
        public fun wasPublished(): Boolean = wasPublished
    }

}