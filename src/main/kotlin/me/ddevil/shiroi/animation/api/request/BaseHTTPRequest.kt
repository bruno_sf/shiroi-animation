package me.ddevil.shiroi.animation.api.request

import me.ddevil.shiroi.misc.request.Result
import me.ddevil.shiroi.misc.request.internal.BaseRequest
import org.apache.http.HttpResponse
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpUriRequest
import org.apache.http.impl.client.HttpClientBuilder

abstract class BaseHTTPRequest<R : Result?> : BaseRequest<R> {
    private final var client: HttpClient

    constructor(client: HttpClient) {
        this.client = client
    }

    constructor() : this(HttpClientBuilder.create().build())

    override fun call0(): R {
        var response = client.execute(createRequest())
        return call1(response);
    }

    abstract fun createRequest(): HttpUriRequest

    abstract fun call1(response: HttpResponse): R
}