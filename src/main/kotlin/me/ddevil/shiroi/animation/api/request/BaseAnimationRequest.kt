package me.ddevil.shiroi.animation.api.request

import me.ddevil.shiroi.misc.request.Result
import org.apache.http.HttpResponse
import org.apache.http.client.HttpClient
import org.json.simple.parser.JSONParser
import java.io.InputStreamReader

abstract class BaseAnimationRequest<R : Result> : BaseHTTPRequest<R> {

    constructor(client: HttpClient) : super(client)

    constructor() : super()

    internal fun loadJson(response: HttpResponse): Map<String, Any> {
        return JSONParser().parse(InputStreamReader(response.entity.content)) as Map<String, Any>
    }
}