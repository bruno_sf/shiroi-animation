package me.ddevil.shiroi.animation.api

import me.ddevil.shiroi.misc.Serializable
import me.ddevil.shiroi.misc.vector.Vector3
import me.ddevil.shiroi.util.item.Item

interface FrameComparison : Serializable {
    fun getDifferentPositions(): Map<ArmorLimb, Vector3<Int>>

    fun getDifferentItems(): Map<ArmorLimb, Item>

    fun getDifferentName(): String?

    fun getDifferentProperties(): Map<ArmorProperty, Boolean>

    fun hasChanged(): Boolean
}