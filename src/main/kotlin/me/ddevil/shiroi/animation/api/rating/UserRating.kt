package me.ddevil.shiroi.animation.api.rating

import me.ddevil.shiroi.misc.Serializable
import java.util.*

interface UserRating : Serializable, Comparable<UserRating> {

    fun getRating(): Rating

    fun getUser(): UUID

    fun getDate(): Date?

}