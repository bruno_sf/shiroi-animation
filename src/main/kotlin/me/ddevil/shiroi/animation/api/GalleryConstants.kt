package me.ddevil.shiroi.animation.api

object GalleryConstants {
    const val API_SUFFIX = "api/animation/"
    const val ANIMATION_API = API_SUFFIX + ":name"
    const val RATING_API = ANIMATION_API + "/rating"
    const val RATING_API_SUFFIX = "/rating"
    const val GALLERY_PORT = 4932
    const val CENTRAL_GALLERY_ADRESS = "http://localhost:$GALLERY_PORT/"
    const val CENTRAL_GALLERY = CENTRAL_GALLERY_ADRESS + API_SUFFIX
    const val USER_IDENTIFIER = "user"
    const val USER_ANIMATIONS_IDENTIFIER = "animations"
    const val USER_RATING_SCORE_IDENTIFIER = "rating"
    const val USER_RATING_DATE_IDENTIFIER = "date"
    const val ANIMATION_RATING_RATINGS_IDENTIFIER = "ratings"
    const val ANIMATION_RATING_NAME_IDENTIFIER = "animation"
}