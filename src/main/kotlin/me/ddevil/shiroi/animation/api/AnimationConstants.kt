package me.ddevil.shiroi.animation.api

object AnimationConstants {
    const val ITEMS_IDENTIFIER = "items"
    const val LIMBS_POSITION_IDENTIFIER = "limbs"
    const val PROPERTIES_IDENTIFIER = "properties"
    const val NAME_IDENTIFIER = "name"
    const val DURATION_IDENTIFIER = "duration"
    const val FRAMES_IDENTIFIER = "frames"
    const val AUTHORS_IDENTIFIER = "authors"
    const val LOCAL_ANIMATION_FILE_EXTENSIONS = ".mcanim"
    const val MINIMUM_FRAME_DURATION = 1
    const val MODEL_POSITION_IDENTIFIER = "position"

}