package me.ddevil.shiroi.animation.api.request

import com.google.common.net.HttpHeaders
import com.google.common.net.MediaType
import me.ddevil.shiroi.animation.api.GalleryConstants
import me.ddevil.shiroi.animation.api.rating.AnimationRating
import me.ddevil.shiroi.animation.api.rating.UserRating
import me.ddevil.shiroi.misc.request.Result
import org.apache.http.HttpResponse
import org.apache.http.HttpStatus
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpPost
import org.apache.http.client.methods.HttpUriRequest
import org.apache.http.entity.StringEntity
import org.json.simple.JSONObject
import java.net.URI

class CastRatingRequest : BaseHTTPRequest<CastRatingRequest.CastRatingResult> {
    private final var animation: String
    private final var rating: UserRating
    override fun createRequest(): HttpUriRequest {
        var request = HttpPost(URI.create(GalleryConstants.CENTRAL_GALLERY + animation + GalleryConstants.RATING_API_SUFFIX));
        request.entity = StringEntity(JSONObject(rating.serialize()).toJSONString())
        request.addHeader(HttpHeaders.CONTENT_TYPE, MediaType.JSON_UTF_8.toString())
        return request
    }

    override fun call1(response: HttpResponse): CastRatingResult {
        if (response.statusLine.statusCode == HttpStatus.SC_NOT_FOUND) {
            return CastRatingResult(false, false);
        }
        return CastRatingResult(true, true)
    }

    constructor(client: HttpClient, animation: String, rating: UserRating) : super(client) {
        this.animation = animation
        this.rating = rating
    }

    constructor(animation: String, rating: UserRating) : super() {
        this.animation = animation
        this.rating = rating
    }


    class CastRatingResult : Result {
        private var wasCast: Boolean
        private var foundAnimation: Boolean

        constructor(wasCast: Boolean, foundAnimation: Boolean) {
            this.wasCast = wasCast
            this.foundAnimation = foundAnimation
        }

        fun wasCast(): Boolean = wasCast
        fun foundAnimation(): Boolean = foundAnimation
    }
}