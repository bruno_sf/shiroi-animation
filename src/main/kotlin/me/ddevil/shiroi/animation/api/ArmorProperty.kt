package me.ddevil.shiroi.animation.api

enum class ArmorProperty {
    BIG,
    GRAVITY,
    BASE_PLATE,
    NAMETAG_VISIBLE;

 }

