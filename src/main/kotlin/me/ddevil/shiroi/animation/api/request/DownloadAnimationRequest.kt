package me.ddevil.shiroi.animation.api.request

import me.ddevil.shiroi.animation.api.Animation
import me.ddevil.shiroi.animation.api.GalleryConstants
import me.ddevil.shiroi.animation.api.rating.AnimationRating
import me.ddevil.shiroi.animation.internal.InternalAnimation
import me.ddevil.shiroi.misc.request.Result
import org.apache.http.HttpResponse
import org.apache.http.HttpStatus
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpUriRequest
import org.json.simple.parser.JSONParser
import java.io.InputStreamReader
import java.net.URI

class DownloadAnimationRequest : BaseAnimationRequest<DownloadAnimationRequest.DownloadAnimationResult> {
    private final var animation: String

    constructor(animation: String) {
        this.animation = animation
    }

    constructor(client: HttpClient, animation: String) : super(client) {
        this.animation = animation
    }

    override fun createRequest(): HttpUriRequest {
        var request = HttpGet(URI.create(GalleryConstants.CENTRAL_GALLERY + animation));
        return request
    }

    override fun call1(response: HttpResponse): DownloadAnimationResult {
        if (response.statusLine.statusCode == HttpStatus.SC_NOT_FOUND) {
            return DownloadAnimationResult(null, null)
        }
        return DownloadAnimationResult(InternalAnimation(loadJson(response)), null)
    }

    class DownloadAnimationResult(private final var animation: Animation?, private final val rating: AnimationRating?) : Result {

        fun getAnimation(): Animation? = animation

        fun getRating(): AnimationRating? = rating

    }
}