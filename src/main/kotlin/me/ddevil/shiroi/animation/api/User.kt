package me.ddevil.shiroi.animation.api

import me.ddevil.shiroi.misc.Serializable
import java.util.*

interface User : Serializable {

    fun getUUID(): UUID

    fun getAnimations(): Array<String>


}