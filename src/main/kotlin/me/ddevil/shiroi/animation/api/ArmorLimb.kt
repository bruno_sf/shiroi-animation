package me.ddevil.shiroi.animation.api

enum class ArmorLimb {
    HEAD,
    LEFT_ARM,
    RIGHT_ARM,
    LEFT_LEG,
    RIGHT_LEG
}
