package me.ddevil.shiroi.animation.api

import me.ddevil.shiroi.misc.Nameable
import java.util.*

interface Animation : Nameable {

    operator fun plus(frame: Frame)

    operator fun get(pos: Int): Frame?

    operator fun set(pos: Int, frame: Frame);

    fun getAuthors(): Array<User>


    fun getTotalFrames(): Int


    fun getFrames(): List<Frame>
}