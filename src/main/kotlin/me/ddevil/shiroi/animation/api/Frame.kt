package me.ddevil.shiroi.animation.api

import me.ddevil.shiroi.misc.Serializable
import me.ddevil.shiroi.misc.vector.Vector3
import me.ddevil.shiroi.util.area.position.SimpleLocation
import me.ddevil.shiroi.util.item.Item

interface Frame : Serializable {


    operator fun set(limb: ArmorLimb, vector3: Vector3<Int>?)

    operator fun get(limb: ArmorLimb): Vector3<Int>?

    operator fun set(property: ArmorProperty, value: Boolean)

    operator fun get(property: ArmorProperty): Boolean

    fun setLocation(location: SimpleLocation?)

    fun getLocation(): SimpleLocation?

    fun setItem(limb: ArmorLimb, item: Item?)

    fun getItem(limb: ArmorLimb): Item?

    fun setName(name: String?)

    fun getName(): String?

    fun compare(frame: Frame): FrameComparison

}


