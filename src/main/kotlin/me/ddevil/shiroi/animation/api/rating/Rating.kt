package me.ddevil.shiroi.animation.api.rating

enum class Rating {
    HORRIBLE,
    BAD,
    NORMAL,
    NICE,
    PERFECT;

    fun getScore(): Int = ordinal
}