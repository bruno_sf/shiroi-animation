package me.ddevil.shiroi.animation.internal.function

import me.ddevil.shiroi.animation.api.ArmorLimb
import java.util.function.Function

class LimbFunction : Function<ArmorLimb, String> {
    override fun apply(t: ArmorLimb?): String? {
        return t?.name;
    }

}
