package me.ddevil.shiroi.animation.internal.rating

import com.google.common.collect.ImmutableMap
import me.ddevil.shiroi.animation.api.GalleryConstants
import me.ddevil.shiroi.animation.api.rating.AnimationRating
import me.ddevil.shiroi.animation.api.rating.UserRating
import me.ddevil.shiroi.util.serialization.SerializationUtils
import java.util.*

class InternalAnimationRating : AnimationRating {
    private final var animationName: String
    private final var ratings: SortedSet<UserRating>

    constructor(animationName: String) {
        this.animationName = animationName
        this.ratings = TreeSet();
    }

    constructor(map: Map<String, Any>) {
        this.animationName = map[GalleryConstants.ANIMATION_RATING_NAME_IDENTIFIER] as String
        var des: Set<UserRating> = SerializationUtils.deserializeCollection(
                map[GalleryConstants.ANIMATION_RATING_RATINGS_IDENTIFIER] as List<Map<String, Any>>,
                {  j -> InternalUserRating(j)}
        );
        this.ratings = TreeSet(des)
    }

    override fun getAnimationName(): String = animationName

    override fun getRatings(): SortedSet<UserRating> = TreeSet<UserRating>(ratings)
    override fun plus(userRating: UserRating) {
        this.ratings.add(userRating)
    }

    override fun serialize(): Map<String, Any> {
        return ImmutableMap.Builder<String, Any>()
                .put(GalleryConstants.ANIMATION_RATING_NAME_IDENTIFIER, animationName)
                .put(GalleryConstants.ANIMATION_RATING_RATINGS_IDENTIFIER, SerializationUtils.serializeCollection(ratings))
                .build();
    }

    override fun getAverageScore(): Float {
        var totalScore = 0;
        ratings.forEach {
            r ->
            totalScore += r.getRating().getScore()
        }
        return totalScore / ratings.size as Float;
    }

}