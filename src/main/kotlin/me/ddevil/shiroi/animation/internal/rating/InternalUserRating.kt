package me.ddevil.shiroi.animation.internal.rating

import com.google.common.collect.ImmutableMap
import me.ddevil.shiroi.animation.api.GalleryConstants
import me.ddevil.shiroi.animation.api.rating.Rating
import me.ddevil.shiroi.animation.api.rating.UserRating
import java.text.SimpleDateFormat
import java.util.*

class InternalUserRating : UserRating {
    override fun compareTo(other: UserRating): Int {
        var od = other.getDate();
        if (date == null) {
            return -1;
        }
        if (od == null) {
            return 1;
        }
        if (od.before(date) ) {
            return -1;
        } else if (od.after(date) ) {
            return 1;
        } else {
            return 0;
        }

    }

    private final var rating: Rating
    private final var uuid: UUID
    private final var date: Date?

    constructor(map: Map<String, Any>) {
        this.rating = Rating.values()[(map[GalleryConstants.USER_RATING_SCORE_IDENTIFIER] as Number).toInt()]
        this.uuid = UUID.fromString(map[GalleryConstants.USER_IDENTIFIER] as String)
        if (map.containsKey(GalleryConstants.USER_RATING_DATE_IDENTIFIER)) {
            date = SimpleDateFormat().parse(map[GalleryConstants.USER_RATING_DATE_IDENTIFIER] as String)
        } else {
            date = null
        }
    }

    constructor(rating: Rating, uuid: UUID) {
        this.rating = rating;
        this.uuid = uuid
        this.date = null
    }

    fun setDate(date: Date) = { this.date = date };

    override fun getUser(): UUID = uuid

    override fun getDate(): Date? = date

    override fun getRating(): Rating = rating

    override fun serialize(): Map<String, Any> {
        var builder = ImmutableMap.Builder<String, Any>()
                .put(GalleryConstants.USER_IDENTIFIER, uuid.toString())
                .put(GalleryConstants.USER_RATING_SCORE_IDENTIFIER, rating.getScore());
        if (date != null) {
            builder.put(GalleryConstants.USER_RATING_DATE_IDENTIFIER, date)
        }
        return builder.build()
    }


}