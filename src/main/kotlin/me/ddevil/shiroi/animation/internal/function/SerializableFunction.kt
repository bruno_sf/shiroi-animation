package me.ddevil.shiroi.animation.internal.function

import me.ddevil.shiroi.misc.Serializable
import me.ddevil.shiroi.util.item.Item
import org.json.simple.JSONObject
import java.util.function.Function

class SerializableFunction : Function<Serializable, JSONObject> {
    override fun apply(t: Serializable?): JSONObject? {
        return JSONObject(t?.serialize())
    }

}