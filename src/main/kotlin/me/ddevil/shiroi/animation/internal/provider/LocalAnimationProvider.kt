package me.ddevil.shiroi.animation.internal.provider

import me.ddevil.shiroi.animation.api.Animation
import me.ddevil.shiroi.animation.api.AnimationConstants
import me.ddevil.shiroi.animation.api.provider.AnimationProvider
import me.ddevil.shiroi.animation.internal.InternalAnimation
import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser
import java.io.*
import java.util.*

class LocalAnimationProvider : AnimationProvider {
    override fun has(animation: String): Boolean {
        return getAnimationFile(animation).exists()
    }

    private final var folder: File

    constructor(folder: File) {
        this.folder = folder
        if (!folder.exists()) {
            folder.mkdirs()
        }
    }

    override fun save(animation: Animation) {
        var file = getAnimationFile(animation.name)
        var writer = BufferedWriter(FileWriter(file));
        var map = HashMap(animation.serialize());
        //todo Serializer when getting home
        var authors: List<UUID> = map[AnimationConstants.AUTHORS_IDENTIFIER] as List<UUID>;
        map.put(AnimationConstants.AUTHORS_IDENTIFIER, authors.map { s -> s.toString() })
        writer.write(JSONObject(map).toJSONString())
        writer.close()
    }

    override fun get(animation: String): Animation? {
        var file = getAnimationFile(animation)
        if (!file.exists()) {
            return null
        }
        return InternalAnimation(JSONParser().parse(BufferedReader(FileReader(file))) as Map<String, Any>)
    }

    fun getAnimationFile(name: String): File = File(folder, name + AnimationConstants.LOCAL_ANIMATION_FILE_EXTENSIONS)

}