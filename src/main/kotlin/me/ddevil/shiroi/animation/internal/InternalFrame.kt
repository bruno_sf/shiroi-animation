package me.ddevil.shiroi.animation.internal

import com.google.common.collect.ImmutableMap
import me.ddevil.shiroi.animation.api.*
import me.ddevil.shiroi.animation.internal.function.SerializableFunction
import me.ddevil.shiroi.animation.internal.function.LimbFunction
import me.ddevil.shiroi.misc.Serializable
import me.ddevil.shiroi.misc.vector.Vector3
import me.ddevil.shiroi.util.area.position.SimpleLocation
import me.ddevil.shiroi.util.item.Item
import me.ddevil.shiroi.util.serialization.SerializationConstants
import me.ddevil.shiroi.util.serialization.SerializationUtils
import org.json.simple.JSONObject
import java.util.*
import java.util.function.Function

class InternalFrame : Frame {


    private final var positionMap: MutableMap<ArmorLimb, Vector3<Int>?>
    private final var itemMap: MutableMap<ArmorLimb, Item?>
    private final var propertyMap: MutableMap<ArmorProperty, Boolean>
    private var location: SimpleLocation?
    private var name: String?

    constructor(map: Map<String, Any>) {
        positionMap = HashMap()
        //Load limbs
        if (map.containsKey(AnimationConstants.LIMBS_POSITION_IDENTIFIER)) {
            positionMap.putAll(SerializationUtils
                    .transformMap(
                            map[AnimationConstants.LIMBS_POSITION_IDENTIFIER] as Map<String, Any>,
                            Function<String, ArmorLimb> { s: String -> ArmorLimb.valueOf(s) },
                            java.util.function.Function<Any, Vector3<Int>> { limbPos ->
                                var limb = limbPos as Map<String, Any>
                                return@Function Vector3(
                                        (limb[SerializationConstants.X_IDENTIFIER] as Number).toInt(),
                                        (limb[SerializationConstants.Y_IDENTIFIER] as Number).toInt(),
                                        (limb[SerializationConstants.Z_IDENTIFIER] as Number).toInt()
                                )

                            })
            )
        }
        //Load items
        itemMap = HashMap()
        if (map.containsKey(AnimationConstants.ITEMS_IDENTIFIER)) {
            var serializedItems = map[AnimationConstants.ITEMS_IDENTIFIER] as Map<String, Any>
            for (i in serializedItems) {
                var limbName = i.key
                var item = i.value as Map<String, Any>
                itemMap.put(ArmorLimb.valueOf(limbName), Item.deserialize(item))
            }
        }

        //load properties
        propertyMap = HashMap()
        if (map.containsKey(AnimationConstants.PROPERTIES_IDENTIFIER)) {
            propertyMap.putAll(SerializationUtils.transformMap(
                    map[AnimationConstants.PROPERTIES_IDENTIFIER] as Map<String, Any>,
                    { s -> ArmorProperty.valueOf(s) },
                    { b -> b as Boolean }
            ))
        }
        name = map[AnimationConstants.NAME_IDENTIFIER] as String?
        if (map.containsKey(AnimationConstants.MODEL_POSITION_IDENTIFIER)) {
            location = SimpleLocation.deserialize(map[AnimationConstants.MODEL_POSITION_IDENTIFIER] as Map<String, Any>)
        } else {
            location = null
        }
    }

    constructor() : this(null, null)

    constructor(location: SimpleLocation?) : this(location, null)

    constructor(modelName: String?) : this(null, modelName)

    constructor(location: SimpleLocation?, name: String?) {
        this.positionMap = HashMap()
        this.itemMap = HashMap()
        this.propertyMap = HashMap()
        this.location = location
        this.name = name
    }


    override fun setLocation(location: SimpleLocation?) {
        this.location = location;
    }


    override fun setName(name: String?) {
        this.name = name
    }

    override fun set(property: ArmorProperty, value: Boolean) {
        propertyMap.put(property, value)
    }

    override fun setItem(limb: ArmorLimb, item: Item?) {
        itemMap.put(limb, item)
    }

    override fun set(limb: ArmorLimb, vector3: Vector3<Int>?) {
        positionMap.put(limb, vector3)
    }

    override fun get(property: ArmorProperty): Boolean {
        return propertyMap[property] ?: false;
    }

    override fun getName(): String? = name

    override fun getLocation(): SimpleLocation? = location

    override fun get(limb: ArmorLimb): Vector3<Int>? = positionMap[limb]

    override fun getItem(limb: ArmorLimb): Item? = itemMap[limb]

    override fun compare(frame: Frame): FrameComparison {
        return InternalFrameComparison(this, frame)
    }

    override fun serialize(): Map<String, Any> {
        val f: Function<ArmorLimb, String> = LimbFunction()
        val a: Function<Serializable, JSONObject> = SerializableFunction()
        var builder = ImmutableMap.Builder<String, Any>()
        if (positionMap.isNotEmpty()) {
            builder.put(AnimationConstants.LIMBS_POSITION_IDENTIFIER, SerializationUtils.transformMap(positionMap as Map<ArmorLimb, Vector3<Int>>, f, a))
        }
        if (itemMap.isNotEmpty()) {
            builder.put(AnimationConstants.ITEMS_IDENTIFIER, SerializationUtils.transformMap(itemMap as Map<ArmorLimb, Item>, f, a))
        }
        if (propertyMap.isNotEmpty()) {
            builder.put(AnimationConstants.PROPERTIES_IDENTIFIER, SerializationUtils.transformMap(propertyMap as Map<ArmorProperty, Boolean>,
                    Function<ArmorProperty, String> { t -> return@Function t.name },
                    Function <Boolean?, Boolean> { t -> return@Function t }))
        }
        if (name != null) {
            builder.put(AnimationConstants.NAME_IDENTIFIER, name)
        }
        if (location != null) {
            builder.put(AnimationConstants.MODEL_POSITION_IDENTIFIER, (location as SimpleLocation).serialize())
        }
        return builder
                .build()
    }
}