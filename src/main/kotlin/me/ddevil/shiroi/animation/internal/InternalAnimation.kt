package me.ddevil.shiroi.animation.internal

import com.google.common.collect.HashBiMap
import com.google.common.collect.ImmutableMap
import me.ddevil.shiroi.animation.api.Animation
import me.ddevil.shiroi.animation.api.AnimationConstants
import me.ddevil.shiroi.animation.api.Frame
import me.ddevil.shiroi.animation.api.User
import me.ddevil.shiroi.misc.internal.BaseNameable
import me.ddevil.shiroi.util.serialization.SerializationUtils
import java.util.*

class InternalAnimation : BaseNameable, Animation {
    private final var frames: MutableMap<Int, Frame>
    private final var authors: Array<User>

    constructor(map: Map<String, Any>) : super(map) {
        var serializedFrames: List<Map<String, Any>> = map[AnimationConstants.FRAMES_IDENTIFIER] as List<Map<String, Any>>
        frames = HashMap()
        serializedFrames.forEach { f -> this + InternalFrame(f) }
        this.authors = (map[AnimationConstants.AUTHORS_IDENTIFIER] as List<User>).toTypedArray()
    }

    constructor(name: String, alias: String, authors: Array<User>) : super(name, alias) {
        frames = HashMap()
        this.authors = authors
    }

    override fun getAuthors(): Array<User> = authors

    override fun getTotalFrames(): Int = frames.size

    override fun set(pos: Int, frame: Frame) {
        frames.put(pos, frame);
    }

    override fun plus(frame: Frame) {
        set(frames.size, frame)
    }

    override fun get(pos: Int): Frame? = frames[pos]

    override fun getFrames(): List<Frame> {
        var bimap: Map<Frame, Int> = HashBiMap.create(frames).inverse()
        val set: List<Frame> = ArrayList(frames.values)
        Collections.sort(set, Comparator { t, a ->
            val one = bimap[t];
            val two = bimap[a];
            if (one != null && two != null) {
                if (one > two) {
                    return@Comparator 1
                } else if (one < two) {
                    return@Comparator -1;
                } else {
                    return@Comparator 0
                }
            } else {
                //This should never be thrown
                throw IllegalStateException("How did you manage to get here?")
            }
        })
        return set
    }

    override fun serialize(): Map<String, Any> {
        var builder = ImmutableMap.Builder<String, Any>().putAll(super.serialize())
        var animFrames = getFrames();
        var currentDuration: Int = AnimationConstants.MINIMUM_FRAME_DURATION
        var serializedFrames: MutableList<Map<String, Any>> = ArrayList()
        val end = animFrames.size - 1
        for (i in 0..end) {
            if (i == end) {
                //Last frame
                serializedFrames.add(animFrames[i].serialize());
                continue
            }
            //Compare to next frame
            var comparison = animFrames[i].compare(animFrames[i + 1])
            if (comparison.hasChanged()) {
                var jsoa = ImmutableMap.Builder<String, Any>()
                        .putAll(comparison.serialize());
                if (currentDuration > AnimationConstants.MINIMUM_FRAME_DURATION) {
                    jsoa.put(AnimationConstants.DURATION_IDENTIFIER, currentDuration)
                }
                serializedFrames.add(jsoa.build());
                currentDuration = AnimationConstants.MINIMUM_FRAME_DURATION;
            } else {
                currentDuration++;
            }
        }
        return builder
                .put(AnimationConstants.FRAMES_IDENTIFIER, serializedFrames)
                .put(AnimationConstants.AUTHORS_IDENTIFIER, authors.map { u -> u.getUUID() })
                .build();

    }
}