package me.ddevil.shiroi.animation.internal

import com.google.common.collect.ImmutableMap
import me.ddevil.shiroi.animation.api.GalleryConstants
import me.ddevil.shiroi.animation.api.User
import java.util.*

class InternalUser : User {
    private final var uuid: UUID
    private final var animations: Array<String>
    constructor(uuid: UUID) : this(uuid, emptyArray())

    constructor(uuid: UUID, animations: Array<String>) {
        this.uuid = uuid
        this.animations = animations
    }


    override fun getUUID(): UUID = uuid
    override fun getAnimations(): Array<String> = animations
    override fun serialize(): Map<String, Any> = ImmutableMap.Builder<String, Any>()
            .put(GalleryConstants.USER_IDENTIFIER, uuid.toString())
            .put(GalleryConstants.USER_ANIMATIONS_IDENTIFIER, animations.asList())
            .build()
}