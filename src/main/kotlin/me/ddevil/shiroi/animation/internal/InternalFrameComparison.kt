package me.ddevil.shiroi.animation.internal

import com.google.common.collect.ImmutableMap
import me.ddevil.shiroi.animation.api.*
import me.ddevil.shiroi.animation.internal.function.SerializableFunction
import me.ddevil.shiroi.animation.internal.function.LimbFunction
import me.ddevil.shiroi.misc.Serializable
import me.ddevil.shiroi.misc.vector.Vector3
import me.ddevil.shiroi.util.item.Item
import me.ddevil.shiroi.util.serialization.SerializationUtils
import org.json.simple.JSONObject
import java.util.*
import java.util.function.Function

class InternalFrameComparison : FrameComparison {

    constructor(firstFrame: Frame, secondFrame: Frame) {

        var positions: MutableMap<ArmorLimb, Vector3<Int>> = HashMap();
        var items: MutableMap<ArmorLimb, Item> = HashMap();
        for (i in ArmorLimb.values()) {
            //Check positions
            val op = firstFrame[i];
            if (op != null) {
                val tp = secondFrame[i];
                if (tp != null && op.equals(tp)) {
                    continue
                }
                positions.put(i, op);
            }
            //Check items
            val oi = firstFrame.getItem(i);
            if (oi != null) {
                val ti = secondFrame.getItem(i);
                if (ti != null && oi.equals(ti)) {
                    continue
                }
                items.put(i, oi);
            }
        }
        var properties: HashMap<ArmorProperty, Boolean> = HashMap();
        for (i in ArmorProperty.values()) {
            val op = firstFrame[i];
            val tp = secondFrame[i];
            if (!op.equals(tp)) {
                properties.put(i, op);
            }
        }

        var differentName: String? = null;
        val firstName = firstFrame.getName()
        val secondName = secondFrame.getName()
        if (firstName != null) {
            if (secondName == null || !firstFrame.equals(secondFrame)) {
                differentName = firstName
            }
        }
        this.differentPositions = positions
        this.differentItems = items
        this.differentProperties = properties
        this.differentName = differentName
    }

    private final var differentPositions: Map<ArmorLimb, Vector3<Int>>
    private final var differentItems: Map<ArmorLimb, Item>
    private final var differentProperties: Map<ArmorProperty, Boolean>
    private final var differentName: String?
    override fun hasChanged(): Boolean = !differentItems.isEmpty() || !differentPositions.isEmpty() || differentProperties.isNotEmpty() || differentName != null


    override fun serialize(): Map<String, Any> {
        var builder = ImmutableMap.Builder<String, Any>();
        val f: Function<ArmorLimb, String> = LimbFunction()
        val a: Function<Serializable, JSONObject> = SerializableFunction()
        if (differentItems.isNotEmpty()) {
            builder.put(AnimationConstants.ITEMS_IDENTIFIER, SerializationUtils.transformMap(differentItems, f, a))
        }
        if (differentPositions.isNotEmpty()) {
            builder.put(AnimationConstants.LIMBS_POSITION_IDENTIFIER, SerializationUtils.transformMap(differentPositions, f, a))
        }
        if (differentProperties.isNotEmpty()) {
            builder.put(AnimationConstants.PROPERTIES_IDENTIFIER, SerializationUtils.transformMap(differentProperties,
                    Function<ArmorProperty, String> { t -> return@Function t.name },
                    Function <Boolean?, Boolean> { t -> return@Function t }))
        }
        if (differentName != null) {
            builder.put(AnimationConstants.NAME_IDENTIFIER, differentName)
        }
        var map = builder.build();
        return map;
    }

    override fun getDifferentPositions(): Map<ArmorLimb, Vector3<Int>> {
        return differentPositions;
    }


    override fun getDifferentItems(): Map<ArmorLimb, Item> {
        return differentItems;
    }


    override fun getDifferentName(): String? {
        return differentName
    }


    override fun getDifferentProperties(): Map<ArmorProperty, Boolean> {
        return differentProperties
    }
}