package me.ddevil.shiroi.animation.internal.provider

import com.mongodb.MongoClient
import com.mongodb.MongoCredential
import com.mongodb.ServerAddress
import com.mongodb.client.MongoCollection
import com.mongodb.client.MongoDatabase
import com.mongodb.client.model.FindOneAndReplaceOptions
import me.ddevil.shiroi.animation.api.Animation
import me.ddevil.shiroi.animation.api.provider.AnimationProvider
import me.ddevil.shiroi.util.serialization.SerializationConstants
import org.bson.Document
import java.net.Inet4Address

class MongoAnimationProvider : AnimationProvider {
    override fun has(animation: String): Boolean {
        return collection.find(createFilter(animation)).first() != null;
    }

    companion object {
        const val PRIMARY_KEY = "_id"
    }

    private final var collection: MongoCollection<Document>
    private var client: MongoClient
    private var database: MongoDatabase
    private final val options = FindOneAndReplaceOptions().upsert(true)

    constructor(client: MongoClient, database: String, collection: String) {
        this.client = client
        this.database = client.getDatabase(database);
        this.collection = this.database.getCollection(collection)

    }

    constructor(address: Inet4Address, database: String, collection: String) : this(MongoClient(ServerAddress(address)), database, collection)

    constructor(address: Inet4Address, username: String, password: String, database: String, collection: String) :
    this(MongoClient(ServerAddress(address),
            listOf(
                    MongoCredential.createCredential(
                            username,
                            database,
                            password.toCharArray()
                    )
            )),
            database,
            collection)


    override fun save(animation: Animation) {
        collection.findOneAndReplace(createFilter(animation.name), createDocument(animation), options)
    }

    private fun createDocument(animation: Animation): Document {
        var doc = Document(animation.serialize());
        return doc.append(PRIMARY_KEY, doc.remove(SerializationConstants.NAMEABLE_NAME_IDENTIFIER))
    }

    private fun createFilter(name: String): Document {
        return Document(PRIMARY_KEY, name)
    }

    override fun get(animation: String): Animation? {
        throw UnsupportedOperationException()
    }
}