import me.ddevil.shiroi.animation.gallery.GalleryServer;
import me.ddevil.shiroi.animation.gallery.rating.LocalRatingProvider;
import me.ddevil.shiroi.animation.internal.provider.LocalAnimationProvider;

import java.io.File;

/**
 * Created by Selma on 21/11/16.
 */
public class RestTest {
    public static void main(String[] args) {
        new GalleryServer(
                new LocalAnimationProvider(new File("/work/shiroi-animation/src/test/resources")),
                new LocalRatingProvider(new File("/work/shiroi-animation/src/test/resources/ratings"))
                ).setup();
    }
}
