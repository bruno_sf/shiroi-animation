package me.ddevil.shiroi.animation.gallery

import com.google.common.collect.ImmutableMap
import com.google.common.net.HttpHeaders
import com.google.common.net.MediaType
import me.ddevil.shiroi.animation.api.provider.AnimationProvider
import me.ddevil.shiroi.animation.api.GalleryConstants
import me.ddevil.shiroi.animation.gallery.rating.RatingProvider
import me.ddevil.shiroi.animation.internal.InternalAnimation
import me.ddevil.shiroi.animation.internal.rating.InternalAnimationRating
import me.ddevil.shiroi.animation.internal.rating.InternalUserRating
import me.ddevil.shiroi.misc.internal.BaseToggleable
import org.apache.http.HttpStatus
import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser
import spark.Spark
import java.util.*
import java.util.logging.Logger
import java.util.logging.SimpleFormatter

class GalleryServer(private final var provider: AnimationProvider, private final var ratingProvider: RatingProvider) : BaseToggleable() {
    companion object {
        val CONTENT_TYPE = MediaType.JSON_UTF_8.toString()
        val LOGGER = getLogger()

        private fun getLogger(): Logger {
            var l = Logger.getGlobal();
            var f = SimpleFormatter();
            l.handlers.forEach { h -> h.formatter = f }
            return l;
        }

    }

    override fun setup0() {
        Spark.port(GalleryConstants.GALLERY_PORT);
        //Animations
        //Download
        Spark.get(GalleryConstants.API_SUFFIX + "/:name") { request, response ->
            var name = request.params(":name");
            var anim = provider[name];
            if (anim == null) {
                response.status(HttpStatus.SC_NOT_FOUND)
                LOGGER.info("Couldn't find animation $name (request from '" + request.ip() + "'");
                return@get getError("Couldn't find any animation with the name '$name'")
            } else {
                LOGGER.info("Returning animation  $name to '" + request.ip() + "'");
                return@get JSONObject(anim.serialize()).toJSONString()
            }
        }
        //Publishing
        Spark.post(GalleryConstants.ANIMATION_API, { request, response ->
            if (!MediaType.parse(request.headers(HttpHeaders.CONTENT_TYPE)).`is`(MediaType.JSON_UTF_8)) {
                return@post getError(HttpHeaders.CONTENT_TYPE + " should be" + CONTENT_TYPE)

            }
            var animationName = request.params(":name");
            if (provider.has(animationName)) {
                response.status(HttpStatus.SC_CONFLICT)
                LOGGER.info("'" + request.ip() + "' attempted to publish an animation which caused a name conflict");
                return@post getError("There is already an animation with this name stored! ($animationName)")
            }
            var json: JSONObject = JSONParser().parse(request.body()) as JSONObject
            var anim = InternalAnimation(json as Map<String, Any>);
            if (anim.getAuthors().isEmpty()) {
                println(json.toJSONString())
                LOGGER.info("'" + request.ip() + "' attempted to publish an animation without any authors");
                response.status(HttpStatus.SC_NOT_ACCEPTABLE)
                return@post getError("There are no authors specified for the given animation!");
            }
            provider.save(anim)
            response.status(HttpStatus.SC_CREATED) ;
            LOGGER.info("'" + request.ip() + "' published an animation ($animationName)");

            return@post getResult("Animation with id '$animationName' successfully published");
        })

        //Ratings
        //AddRating
        Spark.post(GalleryConstants.RATING_API_SUFFIX, { request, response ->
            var name = request.params(":name");
            if (provider.has(name)) {
                LOGGER.info("Couldn't find animation $name to cast rating (request from '" + request.ip() + "'");
                response.status(HttpStatus.SC_NOT_FOUND)
                return@post getError("Couldn't find any animation with the name '$name'")
            }
            var rating = InternalUserRating(JSONParser().parse(request.body()) as Map<String, Any>)
            var animRating = ratingProvider[name];
            if (animRating == null) {
                animRating = InternalAnimationRating(name)
            }
            animRating.plus(rating)
            ratingProvider.save(animRating)
            response.status(HttpStatus.SC_OK)
            return@post getResult("Successfully posted!")
        })
    }

    private fun getResult(msg: String): String = getResult(msg, emptyArray())

    private fun getResult(msg: String, meta: Array<Map.Entry<String, Any>>): String {
        return JSONObject(ImmutableMap.Builder<String, Any>()
                .put("result", msg)
                .putAll(meta.asList())
                .build()).toJSONString()
    }

    private fun getError(msg: String): String {
        return getError(msg, emptyArray())
    }

    private fun getError(msg: String, meta: Array<Map.Entry<String, Any>>): String {
        return JSONObject(ImmutableMap.Builder<String, Any>()
                .put("error", msg)
                .putAll(meta.asList())
                .build()).toJSONString()
    }

    override fun disable0() {
        throw UnsupportedOperationException()
    }

}