package me.ddevil.shiroi.animation.gallery.rating

import me.ddevil.shiroi.animation.api.AnimationConstants
import me.ddevil.shiroi.animation.api.rating.AnimationRating
import me.ddevil.shiroi.animation.internal.rating.InternalAnimationRating
import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser
import java.io.*

class LocalRatingProvider : RatingProvider {
    private final var folder: File

    constructor(folder: File) {
        this.folder = folder
        if (!folder.exists()) {
            folder.mkdirs()
        }
    }

    fun getFile(animation: String): File = File(folder, animation + AnimationConstants.LOCAL_ANIMATION_FILE_EXTENSIONS)


    override fun get(animation: String): AnimationRating? {
        var file = getFile(animation);
        if (file == null) {
            return null
        }
        var reader = BufferedReader(FileReader(file))
        var json = JSONParser().parse(reader.readText()) as JSONObject
        return InternalAnimationRating(json as Map<String, Any>)
    }

    override fun save(animationRating: AnimationRating) {
        var writer = BufferedWriter(FileWriter(getFile(animationRating.getAnimationName())))
        writer.write(JSONObject(animationRating.serialize()).toJSONString())
        writer.close()
    }

}