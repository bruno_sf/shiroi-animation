package me.ddevil.shiroi.animation.gallery.rating

import me.ddevil.shiroi.animation.api.rating.AnimationRating

interface RatingProvider {

    operator fun get(animation: String): AnimationRating?

    fun save(animationRating: AnimationRating)
}